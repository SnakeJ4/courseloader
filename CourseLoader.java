import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class CourseLoader {
	private final static String FILE_PATH = "src/CourseSpreadsheet.csv";

	public static ArrayList<Course> loadCourses() throws FileNotFoundException {
		File csvFile = new File(FILE_PATH);
		Scanner csvReader = new Scanner(csvFile);
		csvReader.nextLine();

		ArrayList<Course> courses = new ArrayList<>();
		while(csvReader.hasNext()){
			String courseString = csvReader.nextLine();
			String[] courseArray = courseString.split(",");
			courses.add(new Course(courseArray[0], courseArray[1],
									Integer.parseInt(courseArray[2]),
									new TimeSlot(intToWeekday(Integer.parseInt(courseArray[3])),
											Integer.parseInt(courseArray[4]),
											Integer.parseInt(courseArray[5]))));
		}
		return courses;
	}

	public static TimeSlot.Weekday intToWeekday(int day){
		switch (day){
			case 1:
				return TimeSlot.Weekday.MONDAY;
			case 2:
				return TimeSlot.Weekday.TUESDAY;
			case 3:
				return TimeSlot.Weekday.WEDNESDAY;
			case 4:
				return TimeSlot.Weekday.THURSDAY;
			case 5:
				return TimeSlot.Weekday.FRIDAY;
			default:
				return null;
		}
	}
}
